package com.demo;

public class Demo1 {

	private int count = 0;
	
	public synchronized void incremnt() {
		count++;
	}

	public static void main(String[] args) throws InterruptedException  {

		Demo1 demo1 = new Demo1();
		demo1.doWork();
	}

	public void doWork() throws InterruptedException  {

		Thread thread = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 1000000; i++) {
					incremnt();
				}

			}
		});

		Thread thread1 = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 100000; i++) {
					incremnt();
				}

			}
		});

		thread.start();
		thread1.start();
		thread1.join();
		thread.join();

		System.out.println("count is :" + count);

	}

}
