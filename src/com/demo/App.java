package com.demo;

import java.util.Scanner;

class Processor extends Thread{
	
	private volatile boolean running=true;

	@Override
	public void run() {
		while (running) {
			
			System.out.println("Hello");
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
	
	public void shutDown() {
		running = false;
	}
	
}

public class App {
	
	public static void main(String[] args) {
		
		Processor processor=new Processor();
		processor.start();
		
		System.out.println("press return to stop...");
		Scanner scanner=new Scanner(System.in);
		scanner.nextLine();
		processor.shutDown();
	}

}
