package com.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Worker {

	private Random random = new Random();
	
	private Object lock1= new Object();
	private Object lock2= new Object();

	private List<Integer> list = new ArrayList<Integer>();
	private List<Integer> list1 = new ArrayList<Integer>();

	public void stageOne() {
		
		synchronized (lock1) {
			

		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		list.add(random.nextInt(100));
		}

	}

	public void stageTwo() {
		
		synchronized (lock2) {

		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		list1.add(random.nextInt(100));
		}

	}

	public void process() {
		for (int i = 0; i < 1000; i++) {
			stageOne();
			stageTwo();
		}
	}

	public void main() {
		System.out.println("Starting..........");
		long start = System.currentTimeMillis();

		Thread thread=new Thread(new Runnable() {

			@Override
			public void run() {
				process();

			}
		});
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		long end = System.currentTimeMillis();
		System.out.println("Time taken  :" + (end - start));
		System.out.println("List  :" + list.size() + "; List1 :" + list1.size());
	}

}
